from random import randint
# player_name = prompt the player for their name
player_name = input("What's your name?")
# use range to repeat following questions 5 times
for guess_number in range(1, 6):
    # month_number = generate a random number
    month_number = randint(1, 12)
    # year_number = generate a random number
    year_number = randint(1924, 2004)

    # print this message:
    #   "Guess " + guess_number + " : " + name + " were you" +
    #   "born on " + month_number + " / " + year_number + " ?"
    print("Guess " + str(guess_number) + " : " + player_name + " were you" +
          "born on " + str(month_number) + " / " + str(year_number) + " ?")
    # response = prompt the player with "yes or no?"
    response = input("yes or no?")

    # if response is "yes", then
    #    print the message "I knew it!"
    if response == "yes":
        print("I knew it!")
    # after the last guess, if incorrect, print message "I have other things to do. Good bye."
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
     # otherwise,
    else:

        #    print the message "Drat! Lemme try again!"
        print("Drat! Lemme try again!")
